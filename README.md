
# Raspberry Pi SHT40 Sensor Data to Django Server
 
Dieses Projekt sammelt Temperatur - und Feuchtigkeitsdaten von einer Wetter API und einem SHT40 Sensor. 
Der Sensor wird von einem Raspberry Pi über I2C ausgelesen. Die Daten der Wetter API, sowie vom Raspberry Pi werden an einen Django-Server gesendet. 
 
## Inhaltsverzeichnis
 
- [Überblick](#überblick)

- [Hardware-Anforderungen](#hardware-anforderungen)

- [Software-Anforderungen](#software-anforderungen)

- [Installation](#installation)

  - [Raspberry Pi Setup](#raspberry-pi-setup)

  - [Django Server Setup](#django-server-setup)

- [Verwendung](#verwendung)

- [Fehlerbehebung](#fehlerbehebung)

- [Projektstruktur](#projektstruktur)


 
## Überblick
 
Dieses Projekt sammelt Temperatur- und Feuchtigkeitsdaten von einem SHT40-Sensor, der an einen Raspberry Pi angeschlossen ist, und sendet die gesammelten Daten an einen Django-Server.
 
## Hardware-Anforderungen
 
- Raspberry Pi (Modell 3 oder neuer empfohlen)

- SHT40-Sensor

- I2C-Verbindungskabel
 
## Software-Anforderungen
 
- Raspberry Pi OS

- Python 3

- Adafruit CircuitPython Libraries

- Django
 
## Installation
 
### Raspberry Pi Setup
 
1. **Update und Upgrade des Raspberry Pi:**

    ```sh

    sudo apt update

    sudo apt upgrade

    ```
 
2. **Installieren der notwendigen Abhängigkeiten:**

    ```sh

    sudo apt install -y python3-pip python3-dev python3-venv git build-essential autoconf automake libtool pkg-config libgpiod2 python3-libgpiod

    ```
 
3. **Erstellen und Aktivieren einer Python-virtuellen Umgebung:**

    ```sh

    python3 -m venv myenv

    source myenv/bin/activate

    ```
 
4. **Installieren der benötigten Python-Pakete:**

    ```sh

    pip install adafruit-circuitpython-sht4x requests

    ```
 
5. **Klone das `libgpiod`-Repository und installiere es:**

    ```sh

    git clone https://github.com/brgl/libgpiod.git

    cd libgpiod

    ./autogen.sh --enable-tools=yes --enable-bindings-python

    make

    sudo make install

    sudo ldconfig

    ```
 
6. **Erstelle ein Python-Skript namens `sensor_data.py` und füge den folgenden Code ein:**

Die Server-URL muss spezifisch angepasst werden auf die Addresse auf welcher der Django-Server läuft. 

    ```python

    import time

    import board

    import adafruit_sht4x

    import requests
 
    # Initialize the SHT40 sensor

    i2c = board.I2C()

    sensor = adafruit_sht4x.SHT4x(i2c)

    sensor.mode = adafruit_sht4x.Mode.NOHEAT_HIGHPRECISION
 
    # Server URL

    server_url = "http://127.0.0.1:8000/sensor_data/"
 
    while True:

        try:

            temperature, humidity = sensor.temperature, sensor.relative_humidity

            print(f"Temperature: {temperature:.2f} C, Humidity: {humidity:.2f} %")
 
            # Send data to Django server

            data = {

                'temperature': temperature,

                'humidity': humidity

            }

            response = requests.post(server_url, data=data)

            print(f"Server response: {response.status_code}, {response.text}")
 
        except Exception as e:

            print(f"Error: {e}")
 
        # Wait before taking another reading

        time.sleep(10)

    ```
 
### Django Server Setup
 
1. **Voraussetzungen installieren:**

    - Python 3.x

    - Git
 
2. **Klonen Sie das Repository:**

    ```bash

    git clone https://gitlab.com/username/raspi_weather_monitor.git

    cd raspi_weather_monitor

    ```
 
3. **Erstellen und Aktivieren einer Python-virtuellen Umgebung:**

    ```sh

    python -m venv env

    source env/bin/activate  # Für Unix/MacOS

    # oder

    env\Scripts\activate  # Für Windows

    ```
 
4. **Django installieren:**

    ```sh

    pip install django

    ```
 
5. **Neues Django-Projekt erstellen:**

    ```sh

    django-admin startproject myproject

    ```
 
6. **Projektstruktur nach dem Erstellen des Projekts:**

    ```plaintext

    myproject/

    ├── manage.py

    └── myproject/

        ├── __init__.py

        ├── settings.py

        ├── urls.py

        ├── wsgi.py

    ```
 
7. **Django-Server starten:**

    ```bash

    python manage.py runserver 0.0.0.0:8000

    ```
 
## Verwendung
 
### Raspberry Pi-Skript starten
 
1. Stellen Sie sicher, dass Ihr Raspberry Pi mit dem Netzwerk verbunden ist.

2. Navigieren Sie auf Ihrem Raspberry Pi zum Projektverzeichnis und starten Sie das Sensordatenskript:

    ```bash

    python sensor_data.py

    ```
 
## Fehlerbehebung
 
- Überprüfen Sie die Verkabelung zwischen dem Raspberry Pi und dem SHT40-Sensor.

- Stellen Sie sicher, dass alle erforderlichen Pakete installiert sind.

- Überprüfen Sie die Netzwerkverbindung zwischen dem Raspberry Pi und dem Django-Server.
 
## Projektstruktur
 
```plaintext

raspi_weather_monitor/

├── main/

│   ├── migrations/

│   ├── static/

│   │   └── main/

│   │       ├── css/

│   │       ├── img/

│   │       └── js/

│   ├── templates/

│   ├── __init__.py

│   ├── admin.py

│   ├── apps.py

│   ├── models.py

│   ├── tests.py

│   └── views.py

├── raspi_weather_monitor/

│   ├── __init__.py

│   ├── asgi.py

│   ├── settings.py

│   ├── urls.py

│   └── wsgi.py

├── sensor_data.py

├── requirements.txt

└── README.md
